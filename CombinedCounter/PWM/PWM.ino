const unsigned char TOP = 3000; // <255
const unsigned char CMR = TOP - 500;

void setup(){
    cli();
    pinMode(9,OUTPUT); // OC1A
    pinMode(11,OUTPUT);
    pinMode(3,OUTPUT);

    // Timer 0
    TCCR0A = 0;
    TCCR0B = 0;

    TCNT0  = 0;
    TIMSK0 = 0;
    
    // Set on compare match clear at TOP
    TCCR0A |= (0x1 << COM0A1) | (0x1 << COM0A0);
    TCCR0A |= (0x1 << COM0B1) | (0x1 << COM0B0);

    //Set mode 7, fast PWM
    TCCR0A |= (0x1 << WGM01) | (0x1 << WGM00);
    TCCR0B |= (0x1 << WGM02) ;

    // Set prescaler 1024
    TCCR0B |=  (0x1 << CS02) | (0x1 << CS00);

    // Enable output compare for ORC1B;
    TCCR0B |= (0x1 << FOC0B);

    OCR0A = TOP;
    OCR0B = CMR;
    sei();
    

}

void loop(){
    // Sadge 
}