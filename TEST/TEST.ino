const short CMRA = 15624; // <65536
const short CMRB = 15624; // <65536
const short CMRC = 15624; // <65536

const unsigned int trigR = 0;
const unsigned int trigG = 1;
const unsigned int trigB = 2;
const unsigned int resolution = 3;

volatile int frame = 0;
volatile int segment = 0;

const int maxFrames = 4;


void setup() {
  cli();
 
 
  // IO config
  pinMode(11,OUTPUT); // Red
  pinMode(10,OUTPUT); // Green
  pinMode(9,OUTPUT); //  Blue

  digitalWrite(11,LOW);
  digitalWrite(10,LOW);
  digitalWrite(9,LOW);

  TCCR1A = 0;
  TCCR1B = 0;
  TCCR1C = 0;

  // Set timer to 0
  TCNT1  = 0;

  // Set compare match registers A and B
  OCR1A = CMRA;
  OCR1B = CMRB;

  // Enable output compare for ORC1A and ORC1B;
  TCCR1C = (0x1 << FOC1A) | (0x1 << FOC1B);

  // Enable timer compare interrupts A and B
  TIMSK1 |= (0x1 << OCIE1A) | (0x1 << OCIE1B);

  //Set prescaler and enable CTC mode
  TCCR1B = (0x1 << CS12) | (0x1 << CS10)| (0x1 << WGM12);

  //Toggle ports on compare match
  
  sei(); 

}


ISR(TIMER1_COMPA_vect){


}

ISR(TIMER1_COMPB_vect){


}

ISR(TIMER1_COMPC_vect){


}


void loop() {
  // put your main code here, to run repeatedly:

}
