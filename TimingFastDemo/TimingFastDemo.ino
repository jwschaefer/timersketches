// J W Schaefer 2021
// Arduino Micro
// Timer Code for Individual Project

// This code is written to generate logical pulses to drive LED arrays for shadowgraph images
// The general operation is controlled by timer1 which is activated upon input capture
// The code will generate pulses for a certain number of frames before resetting

// Define constants


// Segment Length 
const short  CMRA = 882; // <65536

// Pulse Length 
const short CMRB = 159; // <256


const unsigned int trigR = 0;
const unsigned int trigG = 101;
const unsigned int trigB = 202;
const unsigned int resolution = 302;

volatile int frame = 0;
volatile int segment = 0;

const int maxFrames = 120;


void setup() {
  digitalWrite(6,HIGH);
  cli();
 
 
  // Normal IO config
  // All outputs in Port B to allow fast output control
  pinMode(11,OUTPUT); // Red
  pinMode(10,OUTPUT); // Green
  pinMode(9,OUTPUT); //  Blue
  pinMode(8,OUTPUT); //  Indicator
  pinMode(6,OUTPUT); //  Reset

  digitalWrite(11,LOW);
  digitalWrite(10,LOW);
  digitalWrite(9,LOW);
  digitalWrite(8,HIGH);

  // Input for interupt
  pinMode(7,INPUT); //   Trigger

  // Attach interupt to pin 7 change
  attachInterrupt(digitalPinToInterrupt(7), trigger, CHANGE );
  
  // Timer 1 config
  // Setup in CTC mode with OCR1A controling timer resolution 
  // and OCR1B controling pulse length

  // Clear Timer/Counter Control Registers 
  TCCR1A = 0;
  TCCR1B = 0;

  // Set timer to 0
  TCNT1  = 0;
  

  // Set compare match registers A and B
  OCR1A = CMRA;
  OCR1B = CMRB;

  // Enable output compare for ORC1A and ORC1B;
  TCCR1C |= (0x1 << FOC1A) | (0x1 << FOC1B);

  frame = 0;
  segment = 0;

  sei(); 


  
  
}

// Called at the end of a segment
ISR(TIMER1_COMPA_vect){

  // Set pins based on triggers
  PORTB |= ((segment == trigR ) << PB7) | ((segment == trigG ) << PB6) | ((segment == trigB ) << PB5) ;

  segment = (++segment)*int(segment != resolution);

  frame += int(segment == 0);
}

// Called at the start of a segment
ISR(TIMER1_COMPB_vect){

  //Clear RGB Pins
  PORTB &= ~((0x1 << PB7) | (0x1 << PB6) | (0x1 << PB5));

}

// Called on change to pin 7
void trigger() {
  cli();

  // Enable timer compare interrupts A and B
  TIMSK1 = (0x1 << OCIE1A) | (0x1 << OCIE1B);
  
  // Set bits for prescaler + turn on CTC mode
  TCCR1B = (0x1 << CS00) | (0x1 << WGM12);

  //Set pins for triggR|G|B = 0
  PORTB |= ((segment == trigR ) << PB7) | ((segment == trigG ) << PB6) | ((segment == trigB ) << PB5);

  //Clears indicator
  PORTB &= ~(0x1 << PB4);
  sei();
  detachInterrupt(digitalPinToInterrupt(7));
  
  
}

void loop() {
  if ((frame >= maxFrames) & (segment != 0)){
    // Disable interupts
    cli();
    
    // Reset
    PORTB &= ~((0x1 << PB7) | (0x1 << PB6) | (0x1 << PB5));
   
    sei();
    digitalWrite(6,LOW);

    
    };
}
