void setup() {
  // put your setup code here, to run once:
  pinMode(12,OUTPUT);
  pinMode(LED_BUILTIN,OUTPUT);
  digitalWrite(12,HIGH);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(LED_BUILTIN,HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN,LOW);
  delay(1000);
}
