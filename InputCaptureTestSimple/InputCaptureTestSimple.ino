const uint8_t CMR0 = 255; //<256
const uint16_t  CMR1 = 15624; //<65536
volatile int count = 0;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(11,OUTPUT);
  pinMode(7,INPUT);
  pinMode(A2,INPUT);
  pinMode(2,OUTPUT);
  digitalWrite(2,HIGH);
  cli();
  //Configures analogue comparator; 
  
  //Clear Analog Comparator Control and Status Register
  ACSR = 0;
  ACSR |= (0x1 << ACIE); // Analog Comparator Interrupt Enable
  ACSR |= (0x1 << ACIS1) | (0x1 << ACIS0); // Comparator Interrupt on Rising Output Edge

  //Analog Comparator Negative input Multiplexer Enable
  ADCSRB |= (0x1 << ACME);
  ADCSRA &= ~(0x1 << ADEN);

  // Analog Comparator Negative Input -> ADC5
  ADMUX |= (0x1 << MUX2) | ((0x1 << MUX0));
  sei();
}

ISR (ANALOG_COMP_vect){
  cli();
  count = count++;
  sei();
  }


void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(String(count));
  digitalWrite(11,count%1);
}
