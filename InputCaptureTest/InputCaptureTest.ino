const uint8_t CMR0 = 255; //<256
const uint16_t  CMR1 = 15624; //<65536
volatile byte state = 0;

void setup() {
  // put your setup code here, to run once:
  cli();
  Serial.begin(9600);
  pinMode(11,OUTPUT);
  pinMode(2, INPUT);
  attachInterrupt(digitalPinToInterrupt(2), trigger, RISING);
  
  
  //Timer 1
  //_____________________________________________________________________________

  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  
  //Force output compare for ORC1A and ORC1B;
  TCCR1C = (0x1 << FOC1A) | (0x1 << FOC1B);
  
  TCNT1  = 0;//initialize counter value to 0
  
  // set compare match registers
  OCR1A = CMR1;
  OCR1B = CMR0;

  // enable timer compare interrupt
  TIMSK1 = (0x1 << OCIE1A) | (0x1 << OCIE1B);
  sei();

  digitalWrite(11,LOW);

}

void trigger() {
  
  cli();
  digitalWrite(11,HIGH);
  // Set bits for prescaler + turn on CTC mode
  TCCR1B = (0x1 << CS12) | (0x1 << CS10)| (0x1 << WGM12);  
  
  detachInterrupt(digitalPinToInterrupt(2));
  sei();
  
  Serial.println("Triggered");
  
}

ISR(TIMER1_COMPA_vect){
  cli();
  digitalWrite(11,HIGH);
  sei();
}

ISR(TIMER1_COMPB_vect){
  cli();
  digitalWrite(11,LOW);
  sei();
}

void loop() {
  // put your main code here, to run repeatedly:
  
}
