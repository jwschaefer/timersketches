const uint16_t TOP = 200;// <65536
const uint16_t CMRC =  159;

volatile unsigned int segment = 0;
volatile unsigned int frame = 0;
volatile unsigned int resolution = 9;

const unsigned int triggR = 0;
const unsigned int triggG = 3;
const unsigned int triggB = 6;

void setup(){

    cli();

    pinMode(11,OUTPUT); // OC1C - Sig
    //pinMode(4,OUTPUT); // PB6  - R
    //pinMode(3,OUTPUT); //  PB5  - G
    //pinMode(2,OUTPUT); //  PB4  - B

    // Timer 1
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1C = 0;
    TCNT1  = 0;
    TIMSK1 = 0;

    // Set prescaler 1024
    TCCR1B =  (0x1 << CS10) | (0x1 << CS12) ;
    //TCCR1B =  (0x1 << CS10);

    // Set on compare match clear at TOP
    TCCR1A |= (0x1 << COM1C1) ;//| (0x1 << COM1A0);

    //Set mode 14, fast PWM
    TCCR1A |= (0x1 << WGM11) ;
    TCCR1B |= (0x1 << WGM12) | (0x1 << WGM13); 


    // Enable output compare for ORC1C;
    TCCR1C |= (0x1 << FOC1C);

    //Enable overflow interupt
    //TIMSK1 |= (0x1 << TOIE1);

    OCR1C = CMRC;
    ICR1 = TOP;

    sei();

}

ISR(TIMER1_OVF_vect){
    segment++;
    segment = (segment) * int(segment != resolution);
    PORTD = ( int(segment == triggR) << PD4) |
             ( int(segment == triggG) << PD0) | 
             ( int(segment == triggB) << PD1) ;

}

void loop(){
    // Sadge 

}
