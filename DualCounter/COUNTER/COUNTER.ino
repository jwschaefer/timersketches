const byte TOP = 9;
const byte CMRA = 0;
const byte CMRB = 3;
const byte CMRC = 6;

void setup(){
    cli();
    pinMode( 9,OUTPUT); // OC1A
    pinMode(10,OUTPUT); // OC1B
    pinMode(11,OUTPUT); // 0C1C
    pinMode(12,INPUT);  // T1

    // Timer 1
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1C = 0;
    TCNT1  = 0;
    TIMSK1 = 0;

    // Pin operation - set on compare clear on TOP
    TCCR1A |= (0x1 << COM1A1) | (0x1 << COM1A0) |
              (0x1 << COM1B1) | (0x1 << COM1B0) |
              (0x1 << COM1C1) | (0x1 << COM1C0) ;

    // Mode 14 
    TCCR1A |= (0x1 << WGM11);
    TCCR1B |= (0x1 << WGM13) | (0x1 << WGM12) ;

    // External clock on rising edge
    TCCR1B |= (0x1 << CS12) | (0x1 << CS11); // | 
              //(0x1 << CS10) ;

    // Force output compare
    TCCR1C |= (0x1 << FOC1A) | (0x1 << FOC1B) |
              (0x1 << FOC1C) ;

    OCR1A = CMRA;
    OCR1B = CMRB;
    OCR1C = CMRC;
    ICR1 = TOP;

    sei();
 }

void loop(){

}
