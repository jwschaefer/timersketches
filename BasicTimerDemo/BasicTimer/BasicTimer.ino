  const int CMR0 = 255; //<65536
  const int CMR1 = 65535; //<65536
  volatile bool toggle = true;

void setup() {
  Serial.begin(9600);
  pinMode(11, OUTPUT);
  digitalWrite(11,LOW);

  cli();
  
  //Timer 1
  //_____________________________________________________________________________

  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCCR1C = (1 << FOC1A) | (1 << FOC1B);
  TCNT1  = 0;//initialize counter value to 0
  
  // set compare match register for 1hz increments
  OCR1A = CMR1;
  
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  
  // Set bits for prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);  
  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);


  sei();
}

ISR(TIMER1_COMPA_vect){
  toggle = !toggle;
  Serial.println(String(toggle));
  cli();
  digitalWrite(11,int(toggle));
  
  sei();
}

ISR(TIMER1_COMPB_vect){
  cli();
  //toggle = !toggle;
  //digitalWrite(11,HIGH);
  sei();
}

void loop() {

}
