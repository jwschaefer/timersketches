const byte ledPin = 11;
const byte interruptPin = 7;
volatile byte state = LOW;

void setup() {
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  pinMode(interruptPin, INPUT);
  attachInterrupt(digitalPinToInterrupt(interruptPin), blink, RISING );
}

void loop() {
  
  digitalWrite(ledPin, state);
}

void blink() {
  Serial.println("Triggered");
  state = !state;
}
