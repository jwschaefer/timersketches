// J W Schaefer 2021
// Arduino Micro
// Demonstration Timer Code for Individual Project

/*
This code generates signals for the control of pulsed LEDs 

Pulses are generated on pins D11, D10, and D9. The pulse length is 
controlled using the CMRB variable and the timer 1 prescaler bits in the
TCCR1B register. Their seperation is controlled by the CMRA variable and 
the prescaler setup, and their trigger index trigg[R|G|B]. Each pin is set
high once per frame, in an intterupt the start of a "segment", when the  
timer1 register TNCT1 == CMRA. It is then set low in another interupt
when TCNT1 == CMRB. The value in TNCT1  increments with the system clock at a 
maximum frequency of 16 MHz, reulting in ~nanosecond accurate timing.

As only the pulse, segment length, frame resolution, and trigger indicies 
are variable, calculations to determine the aproprate variable values for a 
given framerate and timing scheme must happen prior to compilation. Tools to 
help with these calculations and for the automation for the alteration of 
this code can be found in thethe timerTools module.

- https://JWSchaefer@bitbucket.org/jwschaefer/timertools.git

During setup() the pin configuration is set, plus timer1 and its config 
are fully cleared. Timer 1 is then configured but its interupts and counting 
are disabled, and finaly pin 16 is set high to indicatte the system is ready.

Upon a change to the state of pin 7, interupt INT6 is triggered which enables 
and starts timer 1, disables further interupts from pin 7, clears the indicator, 
and sets the trigger out pin in addition to any R G B pins triggered in the 
first segment.

After TNCT1 reaches the value of CMRB, the TIMER1_COMPB interupt is triggered,
clearing the R G B pins.

Then, when TNCT1 reaches the value of CMRB, the TIMER1_COMPA interupt is 
triggered, incrementing the segment count and possibly the frame count, as 
well as setting any R G B pins assigned to that segment.

When the maximum number of frames is reached, pin 17 is set high causing the 
arduino to reset.


 ___________(R &| G &| B)______________
|                                      |
|                                      |
|                                      |
|                                      |_______________________________________

<-------------------------------------> <------------------------------------->
              0 < TNCT1 < OCRB                    OCRB < TNCT1 < OCRA



*/

// Constants

// Segment Length in clock cycles
// The period of the segment = (1/clockFrequency) * (CMRA+1) * prescaler
const short  CMRA = 3000; // <65536

// Pulse Length in clock cycles
// The pulse period = (1/clockFrequency) * (CMRB+1) * prescaler
const short CMRB = 159; // <65536

// Frame Resolution
// The number of discrete segments each frame is split into
const unsigned int resolution = 9;

// Trigger indicies
// The segment index each LED signal is set high on
const unsigned int trigR = 0;
const unsigned int trigG = 1;
const unsigned int trigB = 2;

// The number of frames to be illuminated
const int maxFrames = 4;

// By combining frame resolutions and framrates,
// minimal drift time drift per frame can be calculated
// A crude function for calculating the best segmentation
// for a given framerate can be found in timerTools.calcs

// Variables for tracking the frames and segments
volatile int frame = 0;
volatile int segment = 0;


void setup() {
  cli();
  // Prevent Reset or camera trigger
  digitalWrite(17,HIGH);
  digitalWrite(8,LOW); 
  
  // Normal IO config
  // All outputs in Port B to allow fast output control
  /*
  pinMode(11,OUTPUT); // Red           PB7
  pinMode(10,OUTPUT); // Green         PB6
  pinMode(9,OUTPUT);  // Blue          PB5
  */

  DDRB = B11111111;
  pinMode(7,INPUT);   // Trigger In    PE6

  /*
  pinMode(8,OUTPUT);  // Trigger out   PB4
  pinMode(17,OUTPUT); // Reset Out     PB0
  pinMode(16,OUTPUT); // Indicator     PB2
  

  pinMode(14,OUTPUT)  // D14 unused byt all PBn pins set to output t
                      // herefore PORB can be assigned directly ->
                      // qicker 
  */
  

  //Set LEDs low 
  digitalWrite(16,LOW) // Indicator
  digitalWrite(11,LOW); // Red
  digitalWrite(10,LOW); // Green
  digitalWrite(9,LOW);  // Blue
  

  // Attach interupt to pin 7/PE6
  EICRB |= (0x1 << ISC60);
  EICRB &= ~(0x1 << ISC61);
  EIMSK |= (0x1 << INT6);


  // Timer 1 config
  // Setup in CTC mode with OCR1A controling timer resolution 
  // and OCR1B controling pulse length

  // Clear Timer/Counter Control Registers 
  TCCR1A = 0;
  TCCR1B = 0;

  // Set timer to 0
  TCNT1  = 0;

  //Clear any previous interupts 
  TIFR1 = 0;
  
  // Set compare match registers A and B
  OCR1A = CMRA;
  OCR1B = CMRB;

  // Enable output compare for ORC1A and ORC1B;
  TCCR1C |= (0x1 << FOC1A) | (0x1 << FOC1B);

  frame = 0;
  segment = 0;

  digitalWrite(16,HIGH); // Indicator

  sei(); 

}


// Called at the end of a segment
ISR(TIMER1_COMPA_vect){

  // Set pins based on triggers
  
  PORTB = ((segment == trigR ) << PB7) | ((segment == trigG ) << PB6) | ((segment == trigB ) << PB5) ;

  segment = (++segment)*int(segment != resolution);

  frame += int(segment == 0);
}

// Called at the start of a segment
ISR(TIMER1_COMPB_vect){

  //Clear RGB Pins keep trigger high
  PORTB = (0x1 << PB4) & ~((0x1 << PB7) | (0x1 << PB6) | (0x1 << PB5));

}

// Called on change to pin 7
ISR(INT6_vect){

  // Set bits for prescaler + turn on CTC mode
  TCCR1B = (0x1 << CS12) | (0x1 << CS10)| (0x1 << WGM12);

  //Set trigger out high and R|G|B if triggR|G|B = 0 
  PORTB = (0x1 << PB4) | (segment == trigR ) << PB7) | ((segment == trigG ) << PB6) | ((segment == trigB ) << PB5);

  // Enable timer compare interrupts A and B
  TIMSK1 = (0x1 << OCIE1A) | (0x1 << OCIE1B);

  // Disables interupts on PIN7
  EIMSK &= ~(0x1 << INT6);
  

}

void loop() {
  if ((frame >= maxFrames) & (segment != 0)){
      cli();
      PORTB = 0x0;
    
    };
}
